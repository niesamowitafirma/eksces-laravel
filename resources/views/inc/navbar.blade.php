<nav class="navbar navbar-expand-md shadow-sm">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse" id="navbarsExampleDefault">

        <div class="input-group">
            <input class="form-control form-control-dark w-90" style="
                background-color: #222222; 
                border-color: white;
            " type="text" placeholder="Search" aria-label="Search" aria-describedby="basic-addon1">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon1" style="
                    background-color: #EF323A; 
                    color: white; 
                    border-color: white;
                ">@</span>
            </div>
        </div>

        <ul class="navbar-nav ml-auto">
            <!-- Authentication Links -->
            @guest
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @endif
            @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="/home">Home</a>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest
        </ul> 

        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" 
            aria-haspopup="true" aria-expanded="false" style="
                color: white;
                background-color: #3B3B3B;
                border-color: #3B3B3B;
            "></button>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton" style="background-color: #3B3B3B; border-color: grey; color: white">
              <a class="dropdown-item" style="background-color: #3B3B3B; color: white" href="/">Home</a>
              <a class="dropdown-item" style="background-color: #3B3B3B; color: white" href="/about">About</a>
              <a class="dropdown-item" style="background-color: #3B3B3B; color: white" href="/services">Services</a>
              <a class="dropdown-item" style="background-color: #3B3B3B; color: white" href="/posts">Blog</a>
            </div>
        </div>

    </div>
</nav>
