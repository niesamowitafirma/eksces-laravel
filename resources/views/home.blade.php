@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" style="background-color: #3b3b3b; border-bottom: none;"><h5>{{ Auth::user()->name }}<h5></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <textarea class="longInput" maxlength="240" cols="3" style="
                    width:100%; 
                    margin:2px; 
                    margin-left:0px; 
                    background-color: #3b3b3b;
                    border: none;
                    color: white;
                    height: 128px;
                    "></textarea>
                    <div class="top" style="margin-top: 20px; margin-bottom: 40px; display: flex; justify-content: space-between;">
                        <h3 style="text-align:center; margin: auto 0;">Your Dashboard</h3>
                        <div class="text-right">
                            <a href="/posts/create" class="btn btn-primary" style="margin-right: 3px;">Create Article</a>
                            <a href="/posts/create" class="btn btn-primary" >Save changes</a>
                        </div>
                    </div>
                    <div style="margin-bottom: 12px; display: flex; justify-content: space-between;">
                        <div class="left">
                            <select id="sort" style="background-color: #222; color: white; border: none; padding: 12px; margin-right: 6px;">
                                <option>date</option>
                                <option>views</option>
                            </select>
                            <select id="order" style="background-color: #222; color: white; border: none; padding: 12px; margin-right: 6px;">
                                <option>descending</option>
                                <option>ascending</option>
                            </select>
                        </div>
                        <div class="right">
                            <input type="text" style="background-color: #222; color: white; border: none; margin-right: 3px; height: 100%;">
                        </div>
                    </div>
                    @if (count($posts) > 0)
                        <table class="table table-striped">
                            @foreach ($posts as $post)
                                <tr style="display: flex;">
                                    <td style="width: 80px"><div style="height: 100%; widht: 100%; background-color: white;"></div></td>
                                    <td style="color: white; flex: 1; display: flex; justify-content: center; flex-direction: column;">{{$post->title}}</div></td>
                                    <td style="display: flex; justify-content: flex-end;">
                                        <a class="btn" style="background-color:#222!important; color: white;" href="/posts/{{$post->id}}/edit" >Edit</a>
                                        {!!Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST'])!!}
                                        {{Form::hidden('_method', 'DELETE')}}
                                        {{Form::submit('X', ['class' => 'btn', 'style' => 'background-color:#222; color: white; margin-left: 8px;'])}}
                                        {!!Form::close()!!}
                                    </td>
                                    <div style="display: block; width: 30px">
                                    
                                    </div>
                                </tr>
                                <tr>
                                    <td style="height: 30px; border: none!important"></td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        <p>You have no posts</p>  
                        <a href="/posts/create" class="btn btn-primary" style="width:100%;">Create Post</a>                      
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
