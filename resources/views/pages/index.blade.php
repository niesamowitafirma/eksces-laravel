@extends('layouts.app')

@section('content')    
    {{-- <div class="jumbotron text-center">
       <h1>Welcome To Laravel!</h1>
        <p>This is the laravel application from the "Laravel From Scratch" YouTube series</p>
        <p><a class="btn btn-primary btn-lg" href="/login" role="button">Login</a> <a class="btn btn-success btn-lg" href="/register" role="button">Register</a></p>
    </div>  --}}
    <div class="jumbotron text-center" style="margin:auto; padding-left: 8px; padding-right: 8px; padding-top: 4px; padding-bottom: 4px;">
        <div class="header">
            <h3>Policja jest brutalna :(</h3>
        </div>
        <div class = "jumbotron" style="width: 100%; padding: 0px; margin: 0px; display: flex;">
            <div class = "intro" style="flex: 1; display: flex;
            flex-direction: column;
            justify-content: space-between;">    
                <div class = "content" style="">
                    <p>Lorem Ipsum jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w. wraz z publikacją arkuszy Letrasetu.</p>
                </div>
                <div class="footer" style="">
                    | created by: DZIKIzWIERZAK | created on: 12.12.2019 | 12 pages | 
                </div>
            </div>
            <div class="picture" style=" flex: 1; height: 100%;">
                <div class="intro" style="height: 300px; width: 100%;">
                    <img src="/storage/cover_images/hongkong.jpg" style="height: 100%; width: 100%;">
                </div>
            </div>
        </div>    
    </div>   
@endsection