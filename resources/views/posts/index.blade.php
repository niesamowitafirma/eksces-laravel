@extends('layouts.app')

@section('content')
    <h1>Posts</h1>


    


    @if (count($posts) > 0)
        @foreach ($posts as $post)
          <div class="jumbotron text-center" style="margin:auto; padding-left: 8px; padding-right: 8px; padding-top: 4px; padding-bottom: 4px; margin-bottom: 18px;">
                <div class="header">
                    <h3 class="card-header" style="background-color:#3b3b3b; border:none;"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h3>
                </div>
                <div class = "jumbotron" style="width: 100%; padding: 0px; margin: 0px; display: flex;">
                    <div class = "intro" style="flex: 1; display: flex;
                    flex-direction: column;
                    justify-content: space-between;">    
                        <div class = "content" style="margin-right: 8px;">
                            <p>Lorem Ipsum jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym. Spopularyzował się w latach 60. XX w. wraz z publikacją arkuszy Letrasetu.</p>
                        </div>
                        <div class="footer" style="">
                            | Written on {{$post->created_at}} | By {{$post->user->name}} | 
                        </div>
                    </div>
                    <div class="picture" style=" flex: 1;">
                        <div style="height: 100%; width: 100%;">
                            <img src="/storage/cover_images/{{$post->cover_image}}" style="width:100%">
                        </div>
                    </div>
                </div>    
            </div>
        @endforeach
        {{$posts->links()}}
    @else
        <p>No posts found</p>
    @endif




@endsection